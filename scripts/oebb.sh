#!/bin/sh
sudo docker run --rm -it -v /usr/bin/socks-gw-docker:/usr/bin/socks-gw -v \
/usr/bin/connect-proxy:/usr/bin/connect-proxy -v `pwd`:`pwd` --env-file \
/etc/environment-docker crops/poky --workdir `pwd` --cmd "source oe-init-build-env && bitbake $@"
