#define SZ 2048
int a[SZ];
int b[SZ];

void simple_loop( double *restrict x, double *restrict y)
{
#pragma clang loop vectorize(assume_safety)
  for (int i = 0; i < SZ; i++)
    {
	y[i] = 3*x[i] + y[i];
    }
}

int main()
{
    simple_loop(a,b);
}
