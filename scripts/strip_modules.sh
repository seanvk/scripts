!/bin/bash

#     su -c 'make INSTALL_MOD_STRIP=1 modules_install && make install'
# After a few tests, remove the old kernel from /boot and /lib/modules directories.
# The INSTALL_MOD_STRIP when set to 1 add a strip --strip-debug when installing the module, which is enough to reduce the size drastically.
# See: INSTALL_MOD_STRIP in Documentation/kbuild/kbuild.txt.

# You could also change the configuration of your initramfs.conf

#Find the file at /etc/initramfs-tools/initramfs.conf

#There is a setting that says MODULES=most this includes most of the modules kn your initrd image.

#Change it to MODULES=dep this makes the initramfs generator guess which modules to include.

find . -name *.ko -exec strip --strip-unneeded {} +
