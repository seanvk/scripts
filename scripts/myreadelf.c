typedef struct {
  uint8  e_ident[16];
  uint16 e_type;
  uint16 e_machine;
  uint32 e_version;
  uint32 e_entry;
  uint32 e_phoff;
  uint32 e_shoff;
  uint32 e_flags;
  uint16 e_ehsize;
  uint16 e_phentsize;
  uint16 e_phnum;
  uint16 e_shentsize;
  uint16 e_shnum;
  uint16 e_shstrndx;
} Elf32Hdr;

void read_elf_header(const char* elfFile, const char* outputFile) {
  struct Elf32Hdr header;

  FILE* file = fopen(elfFile, "rb");
  if(file) {
    // read the header
    fread(&header, 1, sizeof(header), file);

    // check so its really an elf file
    if(header.e_ident[0] == 0x7f &&
       header.e_ident[1] == 'E' &&
       header.e_ident[2] == 'L' &&
       header.e_ident[3] == 'F') {

       // write the header to the output file
       FILE* fout = fopen(outputFile, "wb");
       if(fout) {
         fwrite(&header, 1, sizeof(header), fout);
         fclose(fout);
       }
     }

    // finally close the file
    fclose(file);
  }
}
